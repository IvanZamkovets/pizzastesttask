﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzas
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(@"Hi! Enter top count for most frequently ordered pizzas from standard order storage
and we'll show u that pizzas list.
Top count value should be formatted like 123");

            int topCount = 0;
            string textualCount = Console.ReadLine();
            int attemptsCount = 8;
            while (!Int32.TryParse(textualCount, out topCount))
            {
                if (attemptsCount < 1)
                {
                    Console.WriteLine("You've used all your attempts. please try next time. Goodbye!");
                    return;
                }

                Console.WriteLine("Please enter top count value formatted like 123");
                textualCount = Console.ReadLine();

                --attemptsCount;
            }

            StreamReader reader = new StreamReader("Pizzas.json");
            string textPizzas = reader.ReadToEnd();
            List<Pizza> pizzas = JsonConvert.DeserializeObject<List<Pizza>>(textPizzas);//new List<Pizza>();
            List<PizzaOrderFrequency> orderFrequencies = PizzasParser.GetTopPizzas(pizzas, topCount);

            string textOrderFrequencies = JsonConvert.SerializeObject(orderFrequencies, Formatting.Indented);

            Console.WriteLine($"Here is top {topCount} most popular pizzas:\n");
            Console.WriteLine(textOrderFrequencies);
            Console.WriteLine("Goodbye! Press any key to exit");

            Console.ReadLine();

            //Test();
        }

        private static void Test()
        {
            List<PizzaOrderFrequency> topPizzas = new List<PizzaOrderFrequency>();

            Dictionary<string, PizzaOrderFrequency> orderFrequencies = new Dictionary<string, PizzaOrderFrequency>();

            orderFrequencies["0;1"] = new PizzaOrderFrequency
            {
                OrderFrequency = 1
            };

            topPizzas = orderFrequencies.OrderBy(ofp => ofp.Value.OrderFrequency).Take(1).Select(ofp =>
            {
                ofp.Value.Pizza = new Pizza
                {
                    Toppings = ofp.Key.Split(new char[] { ';' }).ToList()
                };

                return ofp.Value;
            }).ToList();

            ;
        }
    }
}
