﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzas
{
    public class PizzaOrderFrequency
    {
        public Pizza Pizza { get; set; }
        public int OrderFrequency { get; set; }
    }
}
