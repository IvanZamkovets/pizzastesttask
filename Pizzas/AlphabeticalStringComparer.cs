﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzas
{
    public class AlphabeticalStringComparer : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            int result = 0;
            int minLength = Math.Min(x.Length, y.Length);

            int currentPosition = 0;
            foreach (char symbol in x)
            {
                if (symbol < y[currentPosition])
                {
                    result = -1;
                    return result;
                }
                if (symbol > y[currentPosition])
                {
                    result = 1;
                    return result;
                }
                ++currentPosition;
            }

            if (x.Length < y.Length)
            {
                result = -1;
            }
            if (x.Length > y.Length)
            {
                result = 1;
            }

            return result;
        }
    }
}
