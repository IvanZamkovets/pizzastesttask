﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzas
{
   public static class PizzasParser
    {
        private static string _orderFrequencyKeySeparator = ";";

        public static List<PizzaOrderFrequency> GetTopPizzas(List<Pizza> pizzas, int topCount)
        {

            List<PizzaOrderFrequency> topPizzas = new List<PizzaOrderFrequency>();

            Dictionary<string, PizzaOrderFrequency> orderFrequencies = new Dictionary<string, PizzaOrderFrequency>();

            ;

            foreach (Pizza item in pizzas)
            {
                string key = String.Join(_orderFrequencyKeySeparator, item.Toppings.OrderBy(tp => tp, new AlphabeticalStringComparer()).ToList());
                if (orderFrequencies.ContainsKey(key))
                {
                    ++orderFrequencies[key].OrderFrequency;
                    continue;
                }

                ;

                orderFrequencies[key] = new PizzaOrderFrequency()
                {
                    OrderFrequency = 1
                };
            }

            ;

            topPizzas = orderFrequencies.OrderByDescending(ofp => ofp.Value.OrderFrequency).Take(topCount).Select(ofp => 
            {
                ofp.Value.Pizza = new Pizza
                {
                    Toppings = ofp.Key.Split(new char[] { _orderFrequencyKeySeparator.FirstOrDefault() }).ToList()
                };

                return ofp.Value;
            }).ToList();

            return topPizzas;
        }
    }
}
